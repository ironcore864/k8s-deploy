# K8s Deploy

## 0 Pod

```
git checkout step-0
```

## 1 Deployment

```
git checkout step-1
```

## 2 ENV Var

```
git checkout step-2
```

## 3 ConfigMap & ENV Var from ConfigMap

```
git checkout step-3
```

## 4 Service

```
git checkout step-4
```

## 5 Chart

```
git checkout step-5
```

## 6 Reload Pod When Config Changes

```
git checkout step-6
```
